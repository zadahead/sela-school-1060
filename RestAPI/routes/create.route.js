const express = require('express');
const route = express.Router();

const { createFile } = require('../fs');


route.post('/create/:filename', (req, res) => {
    /*
        req.parame => filename
        req.body => content
    */
    const { filename } = req.params;
    const { body } = req;

    createFile(`${filename}.json`, body);
    res.send('created');
})

module.exports = route;