const express = require('express');
const route = express.Router();

const { v4 } = require('uuid');
const { insertItem, readFile, updateItem, removeItem } = require('../fs');
const { postSchema } = require('../dto/todos.schema');
const { validator } = require('../dto/validator');

route.post('/', validator(postSchema), (req, res) => {
    const { body } = req;

    insertItem('todos.json', { ...body, id: v4() });

    res.send('saved');
})

route.get('/', (req, res) => {
    const { userid } = req;
    const json = readFile('todos.json');

    const todos = json.filter(t => t.userId === userid);

    res.send(todos);
})


route.get('/:id', (req, res) => {
    const { id } = req.params;
    const json = readFile('todos.json');
    const item = json.find(i => i.id === id);

    res.send(item);
})


/*
    validatePostTodosTDO 

        validate 'completed' field exist
        validate 'title' field exist
        validate 'title' field is not empty
        validate 'title' field length is less then 15
*/

//DTO
const validateTitle = (req, res, next) => {
    const { body } = req;

    if (!body.title) {
        return res.send('title is missing');
    }

    next();
}

route.put('/:id', validateTitle, (req, res) => {
    const { id } = req.params;
    const { body } = req;

    updateItem('todos.json', id, body);

    res.send('saved');

})

route.delete('/:id', (req, res) => {
    const { id } = req.params;

    removeItem('todos.json', id);

    res.send('removed');

})

module.exports = route;