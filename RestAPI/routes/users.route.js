const express = require('express');

const route = express.Router();
const { v4 } = require('uuid');
const { insertItem, readFile } = require('../fs');

const { validator } = require('../dto/validator');

const { postSchema } = require('../dto/users.schema');

const { signJwt } = require('../lib/jwt');

/*
    POST /users/login BODY { email, password }

        respond userid 
*/

route.get('/', (req, res) => {
    console.log(req.headers);
    const users = readFile('users.json');

    res.send(users)
})

route.post('/login', validator(postSchema), (req, res) => {
    const { email, password } = req.body;

    const users = readFile('users.json');
    const user = users.find(u => u.email === email && u.password === password);

    if (!user) {
        return res.status(404).send('no user');
    }

    var token = signJwt({ userid: user.id });

    return res.send({ token });
})

route.get('/me', (req, res) => {
    const { userid } = req;

    const users = readFile('users.json');

    const user = users.find(u => u.id === userid);

    res.send(user);
    //
})

route.post('/', validator(postSchema), (req, res) => {
    const { body } = req;
    console.log(body);

    insertItem('users.json', { ...body, id: v4() });

    users.push(body);

    res.send('user added');
})

route.get('/:id', (req, res) => {
    const { id } = req.params;

    const users = readFile('users.json');
    const user = users.find(u => u.id == id);

    res.send(user)
})

route.put('/:id', (req, res) => {
    const { id } = req.params;
    const { body } = req;

    const index = users.findIndex(u => u.id === id);
    body.id = id;

    users[index] = body;

    res.send('updated');
})

route.delete('/:id', (req, res) => {
    const { id } = req.params;

    users = users.filter(u => u.id !== id);

    res.send('removed');
})



module.exports = route;