const http = require('http');

const server = http.createServer((req, res) => {
    const { url, method } = req;

    console.log('method', method);


    switch (url) {
        case '/home':
            res.write('welcome home');
            break;
        case '/about':
            res.write('this is about');
            break;
        default:
            res.write('unknown');
            break;
    }
    res.end();
});

server.listen(3000, () => {
    console.log('Server is up on port 3000');
})