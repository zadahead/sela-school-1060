const fs = require('fs');

const createFile = (path, content) => {
    saveFile(path, content);
}

const saveFile = (path, content) => {
    fs.writeFileSync(path, JSON.stringify(content));
}

const readFile = (path) => {
    const content = fs.readFileSync(path, 'utf8');
    const json = JSON.parse(content);

    return json;
}

const insertItem = (path, item) => {
    const json = readFile(path);
    json.push(item);

    saveFile(path, json);
}

const updateItem = (path, id, content) => {
    const json = readFile(path);

    const index = json.findIndex(i => i.id === id);

    json[index] = { ...content, id: id };

    saveFile(path, json);
}

const removeItem = (path, id) => {
    const json = readFile(path);

    const updatedList = json.filter(i => i.id !== id);

    saveFile(path, updatedList);
}

module.exports = {
    createFile,
    readFile,
    insertItem,
    updateItem,
    removeItem
}