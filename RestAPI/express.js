const express = require('express');
const cors = require('cors');

//This is an update!
const { TokenNotValid } = require('./lib/status');
const { verifyJwt } = require('./lib/jwt');

const app = express();

app.use(express.json());

app.use(cors({
    origin: 'http://localhost:3000'
}))

app.use((req, res, next) => {
    const { path } = req;
    if (['/users/login'].includes(path)) {
        return next();
    }

    const { token } = req.headers;

    verifyJwt(token, (err, decoded) => {
        if (err) {
            return next(TokenNotValid());
        }

        const { userid } = decoded;
        req.userid = userid;

        next();
    });
})


app.use('/create', require('./routes/create.route'));
app.use('/users', require('./routes/users.route'));
app.use('/todos', require('./routes/todos.route'));

//Errors 

/*
    {
        success: false,
        message: 'error message'
    }
*/
app.use((err, req, res, next) => {

    res.status(err.code || 500).send({
        success: false,
        message: err.message
    });

})


app.listen(3003, () => {
    console.log('Express is running on port 3003');
})



/*
    GET /users => [...] users
*/