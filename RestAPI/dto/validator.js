const ajv = require("../lib/ajv");
const { BadRequest } = require("../lib/status");

module.exports.validator = (schema) => {
    return (req, res, next) => {
        console.log(req.body);

        const { body } = req;

        const validate = ajv.compile(schema);


        const valid = validate(body)
        if (!valid) {
            return next(BadRequest(validate.errors))
        }

        next();
    };
}