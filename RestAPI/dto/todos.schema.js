

const postSchema = {
    type: "object",
    properties: {
        completed: { type: "boolean" },
        title: { type: "string", maxLength: 15, minLength: 1 }
    },
    required: ["completed", "title"],
    additionalProperties: false,
}



module.exports = {
    postSchema
};