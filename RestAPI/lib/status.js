class Status {
    constructor(code, defaultMessage) {
        this.code = code;
        this.defaultMessage = defaultMessage;

        return this.getStatus;
    }

    getStatus = (message) => {
        return {
            code: this.code,
            message: message || this.defaultMessage
        }
    }
}

const BadRequest = new Status(500, "Some Bad Request");
const InternalServerError = new Status(400, "Some Error");
const TokenNotValid = new Status(400, "Token not valid");

module.exports = {
    BadRequest,
    InternalServerError,
    TokenNotValid
}