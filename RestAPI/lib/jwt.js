const jwt = require('jsonwebtoken');

const secretJwt = 'asdasdasdasd';

const verifyJwt = (token, callback) => {
    jwt.verify(token, secretJwt, callback);
}

const signJwt = (payload) => {
    return jwt.sign(payload, secretJwt, { expiresIn: '10m' })
}

module.exports = {
    verifyJwt,
    signJwt
}