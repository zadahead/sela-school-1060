
const Ajv = require("ajv");
const addFormats = require("ajv-formats")

const ajv = new Ajv({ allErrors: true }) // options can be passed, e.g. {allErrors: true}
addFormats(ajv);


const validatePass = (schema, data) => {
    if (data.length < 8) {
        return false;
    }

    return true;
}

ajv.addKeyword({
    keyword: "pass",
    validate: validatePass,
})



module.exports = ajv;