import React from 'react';
import ReactDOM from 'react-dom/client';

import './index.css';
import { App } from './App';
import { LikesProvider } from './Context/likesContext';
import { CountProvider } from './Context/countContext';
import { Store } from './States/Store';
import { Redux } from './Redux/store';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <CountProvider>
    <LikesProvider>
      <Redux>
        <App />
      </Redux>
    </LikesProvider>
  </CountProvider>
);

