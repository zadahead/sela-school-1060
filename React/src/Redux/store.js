import { configureStore } from '@reduxjs/toolkit'
import { Provider } from 'react-redux'
import counterReducer from './count'

const options = {
    reducer: {
        counter: counterReducer
    },
}
const store = configureStore(options)

export const Redux = ({ children }) => {
    return (
        <Provider store={store}>
            {children}
        </Provider>
    )
}
