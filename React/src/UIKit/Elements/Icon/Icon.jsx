import './Icon.css';

export const Icon = (props) => {
    return <i className={`fas fa-${props.i}`}></i>;
}