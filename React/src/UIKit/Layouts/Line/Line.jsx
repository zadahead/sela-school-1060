import './Line.css';

export const Line = (props) => {
    return (
        <div className={`Line ${props.addClass}`}>
            {props.children}
        </div>
    )
}

export const Rows = (props) => {
    return <Line children={props.children} addClass="rows" />
}