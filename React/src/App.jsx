import { useEffect } from 'react';
import { Line, Rows } from './UIKit/Layouts/Line/Line';
import { useState } from 'react';
import { Btn } from './UIKit/Elements/Btn/Btn';

/*
    use useEffect on start, to fetch data from GET /users/me with axios
    set headers { userId: 1 } on the request
    console.log result / write user name on page. 
*/
export const App = () => {
    const [email, setEmail] = useState('');
    const [password, setPasswrod] = useState('')

    const [user, setUser] = useState(null);

    useEffect(() => {
        tryLogin();
    }, [])

    const tryLogin = async () => {
        const userId = localStorage.getItem('USER-ID');
        if (userId) {
            const user = await getMe(userId);
            setUser(user);
        }
        console.log(userId);
    }

    const handleLogin = async () => {
        try {
            const resp = await fetchLogin();

            const user = await getMe(resp.token);

            console.log(user);
            setUser(user);
            localStorage.setItem('USER-ID', resp.token);
        } catch (error) {
            setUser({
                name: error.message
            });
        }
    }

    const fetchLogin = () => {
        return fetch(`http://localhost:3003/users/login`, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                email,
                password
            })
        })
            .then(response => response.json())
    }

    const getMe = (token) => {
        return fetch(`http://localhost:3003/users/me`, {
            headers: {
                token: token
            }
        })
            .then(response => response.json())
            .catch(err => console.error(err));
    }

    const getTodos = () => {
        const token = localStorage.getItem('USER-ID');

        return fetch(`http://localhost:3003/todos`, {
            headers: {
                token: token
            }
        })
            .then(response => response.json())
            .catch(err => console.error(err));
    }

    const handleLogout = () => {
        setUser(null);
        localStorage.removeItem('USER-ID');
    }

    const handleGetTodos = async () => {
        const todos = await getTodos();
        console.log(todos);
    }

    return (
        <>
            <Rows>
                <h1>Login</h1>
                {user ? (
                    <Rows>
                        <h3>{user.name}</h3>

                        <Line>
                            <Btn onClick={handleLogout}>Logout</Btn>
                            <Btn onClick={handleGetTodos}>Todos</Btn>
                        </Line>
                    </Rows>
                ) :
                    (
                        <Rows>
                            <input value={email} onChange={(e) => setEmail(e.target.value)} placeholder="email" />
                            <input value={password} onChange={(e) => setPasswrod(e.target.value)} placeholder="password" />
                            <Btn onClick={handleLogin}>Login</Btn>
                        </Rows>
                    )
                }
            </Rows>
        </>
    )
}