import { createContext, useState } from "react";


export const countContext = createContext(0);

const Provider = countContext.Provider;

export const CountProvider = ({ children }) => {
    const [count, setCount] = useState(0);

    const handleAdd = (value = 1) => {
        setCount(count + value);
    }

    const handleReduce = (value = 1) => {
        setCount(count - value);
    }

    const value = {
        count,
        setCount,
        handleAdd,
        handleReduce
    }

    return (
        <Provider value={value}>
            {children}
        </Provider>
    )
}