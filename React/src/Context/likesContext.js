import { createContext } from "react";

export const likesContext = createContext(0);

const Provider = likesContext.Provider;

export const LikesProvider = ({ children }) => {
    const likes = 10;
    
    return (
        <Provider value={likes}>
            {children}
        </Provider>
    )
}