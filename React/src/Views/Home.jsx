import { CounterAdd } from "../Components/CounterAdd";
import { CounterDisplay } from "../Components/CounterDisplay";
import { LikesDisplay } from "../Components/LikesDisplay";
import { Btn } from "../UIKit/Elements/Btn/Btn";
import { Line, Rows } from "../UIKit/Layouts/Line/Line";

export const Home = () => {
    const handleClick = () => {
        console.log('Clicked!');
    }

    const isShowHeader = true;

    const renderHeader = () => {
        if (isShowHeader) {
            return <h1>Welcome</h1>
        }
        return null;
    }

    return (
        <>
            <Rows>
                {renderHeader()}
                <Rows>
                    <Line>
                        <CounterAdd />
                        <CounterDisplay />
                    </Line>
                </Rows>
                <LikesDisplay />
                <Line>
                    <h2>Click</h2>
                    <Btn i="user" onClick={handleClick} >Hello!</Btn>
                </Line>
            </Rows>
        </>
    )
}