
//actions
export const addCount = () => {
    return { 
        type: 'ADD_COUNT',
        payload: 1
    }
}

//reducers
export const countReducer = (count = 0, action) => {
    switch (action.type) {
        case 'ADD_COUNT':
            return count + action.payload;
           default:
            return count;
    }
}