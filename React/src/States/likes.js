export const addLikes = () => {
    return {
        type: 'ADD_LIKES',
        payload: 2
    }
}

export const likesReducer = (likes = 0, action) => {
    switch (action.type) {
        case 'ADD_LIKES':
            return likes + action.payload;
          default:
            return likes;
    }
}