import { createStore, combineReducers } from "redux";
import { Provider } from "react-redux";
import { countReducer } from "./count";
import { likesReducer } from "./likes";




const reducers = combineReducers({
    count: countReducer,
    likes: likesReducer
})

const store = createStore(reducers);

export const Store = ({ children }) => {
    return (
        <Provider store={store}>
            {children}
        </Provider>
    )
}