import { useContext } from "react"
import { likesContext } from "../Context/likesContext"

export const LikesDisplay = () => {
    const likes = useContext(likesContext);

    return (
        <div>
            <h1>Likes, {likes}</h1>
        </div>
    )
}