import './Name.css';

export const Name = (props) => {
    return <h2 style={props.style} className="Name">{props.id} : My Name is {props.name} and I am {props.age} years old</h2>;
}
