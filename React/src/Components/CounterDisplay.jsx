import { useContext } from "react"
import { useSelector } from "react-redux"
import { countContext } from "../Context/countContext"

export const CounterDisplay = () => {
    const state = useSelector(state => state);
    console.log(state);

    return (
        <div>
            <h3>Count, 0</h3>
        </div>
    )
}