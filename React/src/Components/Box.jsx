import './Box.css';

export const Box = (props) => {
    return (
        <div className='Box'>
            <h1>{props.header}</h1>
            <h2>{props.title}</h2>
            <div className='content'>
                {props.children}
            </div>
        </div>
    )
}