
import { useContext } from 'react';
import { useDispatch } from 'react-redux';
import { countContext } from '../Context/countContext';
import { increment } from '../Redux/count';
import { addCount } from '../States/count';
import { addLikes } from '../States/likes';
import { Btn } from '../UIKit/Elements/Btn/Btn';
import { Line } from '../UIKit/Layouts/Line/Line';

export const CounterAdd = () => {
    const dispatch = useDispatch();

    const handleDispatch = () => {
        dispatch(increment())
    }

    const handleAddLikes = () => {
        dispatch(addLikes())
    }

    return (
        <Line>
            <Btn onClick={() => handleDispatch()}>Dispatch</Btn>
            <Btn onClick={() => handleAddLikes()}>Likes</Btn>
        </Line>
    )
}