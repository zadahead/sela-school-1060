import { Name } from './Name';

const Welcome = (props) => {
    console.log(props);

    const styleCss = {
        color: 'red',
        backgroundColor: 'yellow'
    }

    return (
        <>
            <h1 style={styleCss}>Welcome</h1>
            {props.children}
            <Name style={styleCss} name={props.name1} id="root" age={12} />
            <Name name={props.name2} id="sss" age={55} />
        </>
    )
}

export default Welcome;