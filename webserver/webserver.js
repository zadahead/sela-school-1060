const express = require('express');
const { readFileSync } = require('fs');

const app = express();

app.use(express.static('public'));

app.get('/', (req, res) => {
    const html = readFileSync('index.html', 'utf-8');

    res.send(html);
})

app.listen(4000, () => {
    console.log('Webserver is listening on port 4000');
})