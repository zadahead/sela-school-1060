console.log('this comes from client!');

const fetchNow = async () => {
    const resp = await fetch('http://localhost:3003/todos');
    const json = await resp.json();

    console.log(json);

    document.getElementById('content').innerHTML = '';

    json.forEach(item => {
        appendItem(item);
    });
}

const addTodo = async () => {
    const value = document.getElementById('myinput').value;
    console.log(value);

    const body = {
        completed: false,
        title: value
    }

    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(body)
    };

    await fetch('http://localhost:3003/todos', requestOptions);

    appendItem(body)
}

const appendItem = (item) => {
    const { title } = item;

    const h2 = document.createElement('h2');
    h2.innerHTML = title;
    document.getElementById('content').append(h2);

}